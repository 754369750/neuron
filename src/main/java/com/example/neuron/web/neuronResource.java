package com.example.neuron.web;

import com.example.neuron.excel.ExcelTable;
import com.example.neuron.excel.ExcelUtil;
import com.example.neuron.service.importService;
import com.example.neuron.service.neuronService;
import com.example.neuron.util.ErrorResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class neuronResource {

//    @Value("${alienlab.upload.path}")
//    String upload_path;

    private final neuronService neuronService;
    private final importService importService;


    public neuronResource(neuronService neuronService,importService importService){
        this.neuronService = neuronService;
        this.importService = importService;

    }

    @Autowired
    ExcelUtil excelUtil;


    @ApiOperation(value="excel上传解析示例")
    @PostMapping("/file/excel/upload")
    public ResponseEntity uploadExcel(@RequestPart("data_file") MultipartFile data_file,@RequestPart("boundary_file") MultipartFile boundary_file, @RequestParam Integer number, HttpServletRequest request){
       // String path=request.getSession().getServletContext().getRealPath(upload_path);
        BufferedOutputStream stream = null;
        double result=0;
        Map<String ,Double> result_map = new HashMap<>();
        if (!data_file.isEmpty() && !boundary_file.isEmpty()) {
            try {
                List<ExcelTable> data_result= excelUtil.readExcelToExcelTable(data_file);
                List<ExcelTable> boundary_result= excelUtil.readExcelToExcelTable(boundary_file);
                if(data_result.size()>0 && boundary_result.size()>0){
                    ExcelTable data_table=data_result.get(0);
                    ExcelTable boundary_table=boundary_result.get(0);
                    switch(number){
                        case 0:
                            System.out.println("0");break;
                        case 1:
                            System.out.println("1");break;
                        case 2:
                            System.out.println("2");
                            result  = importService.kTest(data_table,boundary_table);
                            result_map.put("K段神经元覆盖率",result);
                            break;
                        case 3:
                            System.out.println("3");
                            result  = importService.topkTest(data_table);
                            result_map.put("Top-k神经元覆盖率",result);
                            break;
                        case 4:
                            System.out.println("4");
                            importService.tknpTest(data_table);
                            result_map.put("TkNP",result);
                            break;
                        case 5:
                            System.out.println("5");
                            result  = neuronService.boundaryValueTest(data_table,boundary_table);
                            result_map.put("边界覆盖率",result);
                            break;
                        case 6:
                            System.out.println("6");break;
                        case 7:
                            System.out.println("7");break;
                        case 8:
                            System.out.println("8");
                            result = neuronService.signSignCoverTest(data_table);
                            result_map.put("Sign-sign cover覆盖率",result);
                            break;
                        case 9:
                            System.out.println("9");
                            result = neuronService.distanceSignCover(data_table);
                            result_map.put("Distance-sign cover覆盖率",result);
                            break;
                        case 10:
                            System.out.println("10");
                            result = neuronService.signValueCover(data_table);
                            result_map.put("Sign-alue cover覆盖率",result);
                            break;
                        case 11:
                            System.out.println("11");
                            result = neuronService.distanceValueCover(data_table);
                            result_map.put("Distance-value cover覆盖率",result);
                            break;
                        default:
                            System.out.println("default");break;
                    }

//                    Object excelTeacherLevel=excelEntityConvertor.convertEntity(table, ExcelTeacherLevel.class);
//                    return ResponseEntity.ok(excelTeacherLevel);
                }
//                return ResponseEntity.ok(data_result.get(0).getRows().get(0).getCellList().get(0).getFieldValue());
                return ResponseEntity.ok(result_map);

            } catch (Exception e) {
                stream =  null;
                ErrorResult er=new ErrorResult(e);
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(er);
            }
        } else {
            ErrorResult er = new ErrorResult();
            er.setErrorMessage("文件未上传完整");

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(er);
        }
    }

}
