package com.example.neuron.web;

import com.example.neuron.excel.ExcelConfigUtil;
import com.example.neuron.excel.ExcelTable;
import com.example.neuron.excel.ExcelUtil;
import com.example.neuron.service.newService;
import com.example.neuron.util.ErrorResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class newResource {

    @Autowired
    ExcelUtil excelUtil;

    @Autowired
    ExcelConfigUtil excelConfigUtil;

    private  final newService newService;

    public  newResource(newService newService){
        this.newService = newService;

    }


    @ApiOperation(value="深度学习")
    @PostMapping("/file/excel/neuron")
    public ResponseEntity uploadExcel(@RequestPart("data_file") MultipartFile data_file, @RequestPart("congif_file") MultipartFile congif_file, @RequestPart("boundary_file") MultipartFile boundary_file, @RequestParam Integer number, HttpServletRequest request){
        // String path=request.getSession().getServletContext().getRealPath(upload_path);
        BufferedOutputStream stream = null;
        double result=0;
        Map<String ,Double> result_map = new HashMap<>();
        if (!data_file.isEmpty()) {
            try {
                List<ExcelTable> data_result= excelUtil.readExcelToExcelTable(data_file);
                List<ExcelTable> config_result= excelConfigUtil.readExcelToExcelTable(congif_file);
                List<ExcelTable> boundary_result= excelUtil.readExcelToExcelTable(boundary_file);
                if(data_result.size()>0 ){
                    ExcelTable data_table=data_result.get(0);
                    ExcelTable boundary_table=boundary_result.get(0);
                    ExcelTable config_table=config_result.get(0);
                    switch(number){
                        case 0:
                            System.out.println("0");break;
                        case 1:
                            System.out.println("1");break;
                        case 2:
                            System.out.println("2");

                            break;
                        case 3:
                            System.out.println("3");
                            break;
                        case 4:
                            System.out.println("4");
                            break;
                        case 5:
                            System.out.println("5");
                            break;
                        case 6:
                            System.out.println("6");break;
                        case 7:
                            System.out.println("7");break;
                        case 8:
                            System.out.println("8");
                            result = newService.signSignCoverTest(data_table,config_table);
                            result_map.put("Sign-sign cover覆盖率",result);
                            break;
                        case 9:
                            System.out.println("9");
                           result = newService.distanceSignCover(data_table,config_table);
                            result_map.put("Distance-sign cover覆盖率",result);
                            break;
                        case 10:
                            System.out.println("10");
                           result = newService.signValueCover(data_table,config_table);
                            result_map.put("Sign-alue cover覆盖率",result);
                            break;
                        case 11:
                            System.out.println("11");
                           result = newService.distanceValueCover(data_table,config_table);
                            result_map.put("Distance-value cover覆盖率",result);
                            break;
                        default:
                            System.out.println("default");break;
                    }

//                    Object excelTeacherLevel=excelEntityConvertor.convertEntity(table, ExcelTeacherLevel.class);
//                    return ResponseEntity.ok(excelTeacherLevel);
                }
//                return ResponseEntity.ok(data_result.get(0).getRows().get(0).getCellList().get(0).getFieldValue());
                return ResponseEntity.ok(result_map);

            } catch (Exception e) {
                stream =  null;
                ErrorResult er=new ErrorResult(e);
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(er);
            }
        } else {
            ErrorResult er = new ErrorResult();
            er.setErrorMessage("文件未上传完整");

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(er);
        }
    }



}
