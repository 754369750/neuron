package com.example.neuron.excel;

import java.util.List;

public class ExcelRow {
    //行号
    Long rowIndex;
    //每个单元格
    List<ExcelCell> cellList;

    public Long getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Long rowIndex) {
        this.rowIndex = rowIndex;
    }

    public List<ExcelCell> getCellList() {
        return cellList;
    }

    public void setCellList(List<ExcelCell> cellList) {
        this.cellList = cellList;
    }
}
