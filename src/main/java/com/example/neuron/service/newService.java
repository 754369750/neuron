package com.example.neuron.service;

import com.example.neuron.excel.ExcelCell;
import com.example.neuron.excel.ExcelTable;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class newService {

    public double signSignCoverTest(ExcelTable data_table,ExcelTable config_table){

        List<ExcelCell> config_list =  config_table.getRows().get(0).getCellList();

        //本次数据的总列数
        int sum_column = Integer.parseInt(config_list.get(0).getFieldValue().toString()) ;

        //神经元总对数
        int sum = 0;
        if (config_list.size()>1){
            for (int i=1;i<config_list.size()-1;i++){
                sum +=  Integer.valueOf(config_list.get(i).getFieldValue().toString()) * Integer.valueOf(config_list.get(i+1).getFieldValue().toString()) ;
            }
        }
        //记录符合的神经元对数
        int count =0;
        //记录已经符合条件的神经元对
        int[][] array = new int[sum_column][sum_column];
        for (int a=0;a<sum_column;a++){
            for (int b=0;b<sum_column;b++){
                array[a][b]=0;
            }
        }
        //获取符合条件的条件神经元
        // 得到X1行数据
        for (int j=0;j<data_table.getRowCount();j++){
            List<ExcelCell> data_list1 =  data_table.getRows().get(j).getCellList();
            //得到X2行数据
            for (int m=j+1;m<=data_table.getRowCount();m++){
                List<ExcelCell> data_list2 =  data_table.getRows().get(m).getCellList();
                //记录最后一个满足的列值（神经元）
                int record = 0;
                //初始循环列为0
                int column = 0;
                for (int i=1;i<config_list.size()-1;i++){
                    //记录前一层中负值的个数
                    int first_fushucount = 0;
                    int first = Integer.parseInt(config_list.get(i).getFieldValue().toString()) ;
                    int next = Integer.parseInt(config_list.get(i+1).getFieldValue().toString()) ;
                    //从前一层中找到符合的条件神经元
                    for (int k=column;k<(column+first);k++){
                        //取得X1与X2行中的处于第K列的两个数据
                        double column_data1 = (Double) data_list1.get(k).getFieldValue();
                        double column_data2 = (Double) data_list2.get(k).getFieldValue();
                        //进行比较，看有几个相乘小于0的列（存在两个输入X1,X2，当输入分别为X1和X2时，条件神经元的符号改变。）
                        if (column_data1*column_data2<0){
                            first_fushucount++;
                            record = k;
                        }
                    }
                    //如果X1与X2行只有一列相乘小于0的列（当输入分别为X1,X2时，条件神经元的那一层神经元中，除了条件神经元其他所有神经元的符号都没有改变）
                    if (first_fushucount==1){
                        //根据题意，X1与X2行只能有一个符合条件的条件神经元，获取对应的决策神经元个数（在后一层中）
                        for (int n=(column+first);n<(column+first+next);n++){
                            //在第二层中取得X1与X2中的某一列数据
                            double column_decision_data1 = (Double)data_list1.get(n).getFieldValue();
                            double column_decision_data2 = (Double)data_list2.get(n).getFieldValue() ;
                            //进行比较（当输入分别为X1,X2时，决策神经元的符号改变。）
                            if (column_decision_data1*column_decision_data2<0){
                                //找到符合的一列，即多一对符合的神经元对
                                if (array[record][n]==0){
                                    count++;
                                    array[record][n]=1;
                                }
                            }
                        }
                    }
                    column += Integer.parseInt(config_list.get(i).getFieldValue().toString()) ;
                }
            }
        }
        System.out.println(count);
        return (double) count/sum;
    }

    public  double distanceSignCover (ExcelTable data_table,ExcelTable config_table){
        List<ExcelCell> config_list =  config_table.getRows().get(0).getCellList();

        //本次数据的总列数
        int sum_column = Integer.parseInt(config_list.get(0).getFieldValue().toString()) ;

        //神经元总对数
        int sum = 0;
        if (config_list.size()>1){
            for (int i=1;i<config_list.size()-1;i++){
                sum +=  Integer.valueOf(config_list.get(i).getFieldValue().toString()) * Integer.valueOf(config_list.get(i+1).getFieldValue().toString()) ;
            }
        }



        //记录总的符合的对数
        int sum_count=0;

        int [] record = new int[sum_column];

        //获取符合条件的条件神经元
        // 得到X1行数据
        for (int j=0;j<data_table.getRowCount();j++){
            List<ExcelCell> data_list1 =  data_table.getRows().get(j).getCellList();
            //得到X2行数据
            for (int m=j+1;m<=data_table.getRowCount();m++){
                List<ExcelCell> data_list2 =  data_table.getRows().get(m).getCellList();

                //初始循环列为0
                int column = 0;

                //从配置文件的第二列开始遍历直到倒数第二列结束
                for (int i=1;i<config_list.size()-1;i++){
                    //记录前一层中负值的个数
                    int first_fushucount = 0;

                    //记录后一层符合的决策神经元的个数
                    int count =0;

                    List<Double> list1 = new ArrayList<>();
                    List<Double> list2 = new ArrayList<>();

                    int first = Integer.parseInt(config_list.get(i).getFieldValue().toString()) ;
                    int next = Integer.parseInt(config_list.get(i+1).getFieldValue().toString()) ;
                    //从前一层中找到符合的条件神经元
                    for (int k=column;k<(column+first);k++){

                        //取得X1与X2行中的处于第K列的两个数据
                        double column_data1 = (Double) data_list1.get(k).getFieldValue();
                        double column_data2 = (Double) data_list2.get(k).getFieldValue();
                        //进行比较，看有几个相乘小于0的列（存在两个输入X1,X2，当输入分别为X1和X2时，条件神经元的符号改变。）
                        if (column_data1*column_data2<0){
                            first_fushucount++;
                        }
                    }
                    //如果X1与X2行没有一列相乘小于0的列（当输入为X1，X2时，条件神经元的这一层所有神经元的符号均未改变）
                    if (first_fushucount==0){
                        //集合相减,计算L1
                        int sumcount = 0;
                        for (int a =0;a<list1.size();a++){
                            double b = list1.get(a)-list2.get(a);
                            if (b<0){
                                b = -b;
                            }
                            sumcount+=b;
                        }
                        if (sumcount>=0 && sumcount<=50){
                            //根据题意获取对应的决策神经元个数（在后一层中）
                            for (int n=(column+first);n<(column+first+next);n++){
                                //在后一层中取得X1与X2中的某一列数据
                                double column_decision_data1 = (Double)data_list1.get(n).getFieldValue();
                                double column_decision_data2 = (Double)data_list2.get(n).getFieldValue() ;
                                //进行比较（当输入分别为X1,X2时，决策神经元的符号改变。）
                                if (column_decision_data1*column_decision_data2<0){
                                    //找到符合的一列，即多一对符合的神经元对，需要去重
                                    if (record[n]==0){
                                        count++;
                                        record[n]=1;
                                    }
                                }
                            }
                        }
                    }
                    column += Integer.parseInt(config_list.get(i).getFieldValue().toString()) ;
                    sum_count += first*count;
                }
            }
        }
//        System.out.println(second_count);
        System.out.println(sum_count);

        return (double)sum_count/sum;
    }

    public  double signValueCover(ExcelTable data_table,ExcelTable config_table){

        List<ExcelCell> config_list =  config_table.getRows().get(0).getCellList();

        //本次数据的总列数
        int sum_column = Integer.parseInt(config_list.get(0).getFieldValue().toString()) ;

        //神经元总对数
        int sum = 0;
        if (config_list.size()>1){
            for (int i=1;i<config_list.size()-1;i++){
                sum +=  Integer.valueOf(config_list.get(i).getFieldValue().toString()) * Integer.valueOf(config_list.get(i+1).getFieldValue().toString()) ;
            }
        }
        //记录符合的神经元对数
        int count =0;
        //记录已经符合条件的神经元对
        int[][] array = new int[sum_column][sum_column];
        for (int a=0;a<sum_column;a++){
            for (int b=0;b<sum_column;b++){
                array[a][b]=0;
            }
        }
        //获取符合条件的条件神经元
        // 得到X1行数据
        for (int j=0;j<data_table.getRowCount();j++){
            List<ExcelCell> data_list1 =  data_table.getRows().get(j).getCellList();
            //得到X2行数据
            for (int m=j+1;m<=data_table.getRowCount();m++){
                List<ExcelCell> data_list2 =  data_table.getRows().get(m).getCellList();
                //记录最后一个满足的列值（神经元）
                int record = 0;
                //初始循环列为0
                int column = 0;
                for (int i=1;i<config_list.size()-1;i++){
                    //记录前一层中负值的个数
                    int first_fushucount = 0;
                    int first = Integer.parseInt(config_list.get(i).getFieldValue().toString()) ;
                    int next = Integer.parseInt(config_list.get(i+1).getFieldValue().toString()) ;
                    //从前一层中找到符合的条件神经元
                    for (int k=column;k<(column+first);k++){
                        //取得X1与X2行中的处于第K列的两个数据
                        double column_data1 = (Double) data_list1.get(k).getFieldValue();
                        double column_data2 = (Double) data_list2.get(k).getFieldValue();
                        //进行比较，看有几个相乘小于0的列（存在两个输入X1,X2，当输入分别为X1和X2时，条件神经元的符号改变。）
                        if (column_data1*column_data2<0){
                            first_fushucount++;
                            record = k;
                        }
                    }
                    //如果X1与X2行只有一列相乘小于0的列（当输入分别为X1,X2时，条件神经元的那一层神经元中，除了条件神经元其他所有神经元的符号都没有改变）
                    if (first_fushucount==1){
                        //根据题意，X1与X2行只能有一个符合条件的条件神经元，获取对应的决策神经元个数（在后一层中）
                        for (int n=(column+first);n<(column+first+next);n++){
                            //在第二层中取得X1与X2中的某一列数据
                            double column_decision_data1 = (Double)data_list1.get(n).getFieldValue();
                            double column_decision_data2 = (Double)data_list2.get(n).getFieldValue() ;
                            //进行比较（当输入分别为X1,X2时，决策神经元的值改变的足够大且符号未变）
                            if (column_decision_data1*column_decision_data2>0){
                                if (column_decision_data1/column_decision_data2>2 || column_decision_data1/column_decision_data2<0.5){
                                    //找到符合的一列，即多一对符合的神经元对
                                    if (array[record][n]==0){
                                        count++;
                                        array[record][n]=1;
                                    }
                                }
                            }
                        }
                    }
                    column += Integer.parseInt(config_list.get(i).getFieldValue().toString()) ;
                }
            }
        }
        System.out.println(count);
        return (double) count/sum;

    }

    public  double distanceValueCover(ExcelTable data_table,ExcelTable config_table){

        List<ExcelCell> config_list =  config_table.getRows().get(0).getCellList();

        //本次数据的总列数
        int sum_column = Integer.parseInt(config_list.get(0).getFieldValue().toString()) ;

        //神经元总对数
        int sum = 0;
        if (config_list.size()>1){
            for (int i=1;i<config_list.size()-1;i++){
                sum +=  Integer.valueOf(config_list.get(i).getFieldValue().toString()) * Integer.valueOf(config_list.get(i+1).getFieldValue().toString()) ;
            }
        }
        //记录总的符合的对数
        int sum_count=0;

        //记录已经符合条件的神经元对
        int [] record = new int[sum_column];

        //获取符合条件的条件神经元
        // 得到X1行数据
        for (int j=0;j<data_table.getRowCount();j++){
            List<ExcelCell> data_list1 =  data_table.getRows().get(j).getCellList();
            //得到X2行数据
            for (int m=j+1;m<=data_table.getRowCount();m++){
                List<ExcelCell> data_list2 =  data_table.getRows().get(m).getCellList();

                //初始循环列为0
                int column = 0;

                //从配置文件的第二列开始遍历直到倒数第二列结束
                for (int i=1;i<config_list.size()-1;i++){
                    //记录前一层中负值的个数
                    int first_fushucount = 0;

                    //记录后一层符合的决策神经元的个数
                    int count =0;

                    List<Double> list1 = new ArrayList<>();
                    List<Double> list2 = new ArrayList<>();

                    int first = Integer.parseInt(config_list.get(i).getFieldValue().toString()) ;
                    int next = Integer.parseInt(config_list.get(i+1).getFieldValue().toString()) ;
                    //从前一层中找到符合的条件神经元
                    for (int k=column;k<(column+first);k++){

                        //取得X1与X2行中的处于第K列的两个数据
                        double column_data1 = (Double) data_list1.get(k).getFieldValue();
                        list1.add(column_data1);
                        double column_data2 = (Double) data_list2.get(k).getFieldValue();
                        list2.add(column_data2);
                        //进行比较，看有几个相乘小于0的列（存在两个输入X1,X2，当输入分别为X1和X2时，条件神经元的符号改变。）
                        if (column_data1*column_data2<0){
                            first_fushucount++;
                        }
                    }
                    //如果X1与X2行没有一列相乘小于0的列（当输入为X1，X2时，条件神经元的这一层所有神经元的符号均未改变）
                    if (first_fushucount==0){
                        //集合相减,计算L1
                        int sumcount = 0;
                        for (int a =0;a<list1.size();a++){
                            double b = list1.get(a)-list2.get(a);
                            if (b<0){
                                b = -b;
                            }
                            sumcount+=b;
                        }
                        if (sumcount>=0 && sumcount<=50){
                            //根据题意获取对应的决策神经元个数（在后一层中）
                            for (int n=(column+first);n<(column+first+next);n++){
                                //在后一层中取得X1与X2中的某一列数据
                                double column_decision_data1 = (Double)data_list1.get(n).getFieldValue();
                                double column_decision_data2 = (Double)data_list2.get(n).getFieldValue() ;

                                //进行比较（当输入分别为X1,X2时，决策神经元的值改变的足够大且符号未变）
                                if (column_decision_data1*column_decision_data2>0){
                                    if (column_decision_data1/column_decision_data2>5 || column_decision_data1/column_decision_data2<0.2){
                                        //找到符合的一列，即多一对符合的神经元对
                                        if (record[n]==0){
                                            count++;
                                            record[n]=1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    column += Integer.parseInt(config_list.get(i).getFieldValue().toString()) ;

                    sum_count += first*count;
                }
            }
        }
        System.out.println(sum_count);

        return (double)sum_count/sum;
    }

}
