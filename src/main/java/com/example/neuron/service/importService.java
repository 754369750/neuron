package com.example.neuron.service;

import com.example.neuron.excel.ExcelCell;
import com.example.neuron.excel.ExcelTable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Service
@Transactional
public class importService {


    public double kTest(ExcelTable data_table, ExcelTable boundary_table){

        double count1 = (double) 0;
        double count = (double) 0;
        List<ExcelCell>  boundary_list1 = boundary_table.getRows().get(0).getCellList();
        List<ExcelCell>  boundary_list2 = boundary_table.getRows().get(1).getCellList();
        double [] record1 = new double[100];
        for (int i = 0; i < 120; i++) {
            for (int a = 0; a < 100; a++) {
                for (int j=0;j<=data_table.getRowCount();j++) {
                    List<ExcelCell>   data_list1 = data_table.getRows().get(j).getCellList();
                    double column_decision_data1 = (Double)data_list1.get(a).getFieldValue();
                    record1[j] = column_decision_data1;
                }
            }

            double topDouble = (Double) boundary_list1.get(i).getFieldValue();
            double behindDouble = (Double) boundary_list2.get(i).getFieldValue();
            double averageDouble = (Double)((topDouble-behindDouble)/50);
            double leftDouble = 0;
            double rightDouble = 0;
            java.util.Arrays.sort(record1);
            for(int k = 0; k < 50; k++ ){
                leftDouble = behindDouble;
                rightDouble = behindDouble + averageDouble;
                behindDouble = rightDouble;
                for(int p = 0;  p < 100; p++){
                    if (record1[p] <= rightDouble && record1[p] >= leftDouble) {
                        count++;
                    }
                }
                if (count >= 1) {
                    count1++;
                    count = (double) 0;
                }
            }

        }

        System.out.println(count1/(120*50));


        return 0;
    }

    public double topkTest(ExcelTable data_table){
        double [] record1 = new double[120];
        List<Integer> top_1 = new ArrayList<Integer>();
        List<Integer> top_2 = new ArrayList<>();
        List<Integer> top_3 = new ArrayList<>();
        for (int j=0;j<=data_table.getRowCount();j++) {
            List<ExcelCell> data_list1 = data_table.getRows().get(j).getCellList();
            for (int i = 0; i < data_list1.size(); i++) {
                record1[i] = (Double)data_list1.get(i).getFieldValue();
            }
            double [] record2 = record1.clone();
            java.util.Arrays.sort(record1,0,50);
            java.util.Arrays.sort(record1,50,90);
            java.util.Arrays.sort(record1,90,120);

            for(int k = 3; k > 0; k--){
                for(int i = 0; i < 50; i++){
                    if (record2[i] == record1[50-k]) {
                        top_3.add(i);
                    }
                }
                for(int i = 50; i < 90; i++){
                    if (record2[i] == record1[90-k]) {
                        top_3.add(i);
                    }
                }
                for(int i = 90; i < 120; i++){
                    if (record2[i] == record1[120-k]) {
                        top_3.add(i);
                    }
                }
            }
            for(int k = 2; k > 0; k--){
                for(int i = 0; i < 50; i++){
                    if (record2[i] == record1[50-k]) {
                        top_2.add(i);
                    }
                }
                for(int i = 50; i < 90; i++){
                    if (record2[i] == record1[90-k]) {
                        top_2.add(i);
                    }
                }
                for(int i = 90; i < 120; i++){
                    if (record2[i] == record1[120-k]) {
                        top_2.add(i);
                    }
                }
            }
            for(int k = 1; k > 0; k--){
                for(int i = 0; i < 50; i++){
                    if (record2[i] == record1[50-k]) {
                        top_1.add(i);
                    }
                }
                for(int i = 50; i < 90; i++){
                    if (record2[i] == record1[90-k]) {
                        top_1.add(i);
                    }
                }
                for(int i = 90; i < 120; i++){
                    if (record2[i] == record1[120-k]) {
                        top_1.add(i);
                    }
                }

            }
            System.out.println();
            System.out.println("排序后的数组为：");
            for(Double num:record1){
                System.out.print(num+" ");
            }

        }
        System.out.println();
        HashSet<Integer> h1 = new HashSet<Integer>(top_1);
        top_1.clear();
        top_1.addAll(h1);
        HashSet<Integer> h2 = new HashSet<Integer>(top_2);
        top_2.clear();
        top_2.addAll(h2);
        HashSet<Integer> h3 = new HashSet<Integer>(top_3);
        top_3.clear();
        top_3.addAll(h3);
        System.out.print("Top-3神经元覆盖率为：");
        Double top_3Double = (double) top_3.size();
        System.out.println(top_3Double/120);
        System.out.print("Top-2神经元覆盖率为：");
        Double top_2Double = (double) top_2.size();
        System.out.println(top_2Double/120);
        System.out.print("Top-1神经元覆盖率为：");
        Double top_1Double = (double) top_1.size();
        System.out.println(top_1Double/120);



        return 0;

    }

    public double tknpTest(ExcelTable data_table){
        double [] record1 = new double[120];

        List<String> list1 = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();
        List<String> list3 = new ArrayList<String>();
        for (int j=0;j<=data_table.getRowCount();j++) {
            List<Integer> top_1 = new ArrayList<Integer>();
            List<Integer> top_2 = new ArrayList<>();
            List<Integer> top_3 = new ArrayList<>();
            List<ExcelCell> data_list1 = data_table.getRows().get(j).getCellList();
            for (int i = 0; i < data_list1.size(); i++) {
                record1[i] = (Double)data_list1.get(i).getFieldValue();
            }
            double [] record2 = record1.clone();
            java.util.Arrays.sort(record1,0,50);
            java.util.Arrays.sort(record1,50,90);
            java.util.Arrays.sort(record1,90,120);

            for(int k = 3; k > 0; k--){
                for(int i = 0; i < 50; i++){
                    if (record2[i] == record1[50-k]) {
                        top_3.add(i);
                    }
                }
                for(int i = 50; i < 90; i++){
                    if (record2[i] == record1[90-k]) {
                        top_3.add(i);
                    }
                }
                for(int i = 90; i < 120; i++){
                    if (record2[i] == record1[120-k]) {
                        top_3.add(i);
                    }
                }
            }
            for(int k = 2; k > 0; k--){
                for(int i = 0; i < 50; i++){
                    if (record2[i] == record1[50-k]) {
                        top_2.add(i);
                    }
                }
                for(int i = 50; i < 90; i++){
                    if (record2[i] == record1[90-k]) {
                        top_2.add(i);
                    }
                }
                for(int i = 90; i < 120; i++){
                    if (record2[i] == record1[120-k]) {
                        top_2.add(i);
                    }
                }
            }
            for(int k = 1; k > 0; k--){
                for(int i = 0; i < 50; i++){
                    if (record2[i] == record1[50-k]) {
                        top_1.add(i);
                    }
                }
                for(int i = 50; i < 90; i++){
                    if (record2[i] == record1[90-k]) {
                        top_1.add(i);
                    }
                }
                for(int i = 90; i < 120; i++){
                    if (record2[i] == record1[120-k]) {
                        top_1.add(i);
                    }
                }

            }

            Collections.sort(top_3);
            System.out.println(top_3);
            Collections.sort(top_2);
            Collections.sort(top_1);
            String top_3String = "";
            for (int i = 0; i < top_3.size(); i++) {
                top_3String = top_3String + String.valueOf(top_3.get(i));
        }
            String top_2String = "";
            for (int i = 0; i < top_2.size(); i++) {
                top_2String = top_2String + String.valueOf(top_2.get(i));
            }
            String top_1String = "";
            for (int i = 0; i < top_1.size(); i++) {
                top_1String = top_1String + String.valueOf(top_1.get(i));
            }
            System.out.println(top_3String);
            list3.add(top_3String);
            list2.add(top_2String);
            list1.add(top_1String);

        }


        HashSet<String> h3 = new HashSet<String>(list3);
        list3.clear();
        list3.addAll(h3);
        HashSet<String> h2 = new HashSet<String>(list2);
        list2.clear();
        list2.addAll(h2);
        HashSet<String> h1 = new HashSet<String>(list1);
        list1.clear();
        list1.addAll(h1);
        System.out.print("k=3时TKNP=");
        System.out.println(list3.size());
        System.out.print("k=2时TKNP=");
        System.out.println(list2.size());
        System.out.print("k=1时TKNP=");
        System.out.println(list1.size());




        return 0;

    }


}
